#include <Windows.h>

#include <fstream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>

#include <nlohmann/json.hpp>

using json = nlohmann::json;
json module_data, temp_module_data;

void dump_module(HMODULE mod, json& data) {

  auto dos_hd = reinterpret_cast<PIMAGE_DOS_HEADER>(mod);
  auto nt_hd = reinterpret_cast<PIMAGE_NT_HEADERS>(reinterpret_cast<PBYTE>(mod) 
    + dos_hd->e_lfanew);

  data["Base"] = reinterpret_cast<std::uintptr_t>(mod);

  auto export_data_dir = nt_hd->OptionalHeader
    .DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
  auto export_data_size = nt_hd->OptionalHeader
    .DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;

  if (export_data_dir) {

    auto export_section = reinterpret_cast<IMAGE_EXPORT_DIRECTORY*>(
      reinterpret_cast<PBYTE>(mod) + export_data_dir);

    std::uint16_t* ordinals = reinterpret_cast<std::uint16_t*>(export_section->AddressOfNameOrdinals 
      + reinterpret_cast<std::uintptr_t>(mod));
    std::uint32_t* names = reinterpret_cast<std::uint32_t*>(export_section->AddressOfNames 
      + reinterpret_cast<std::uintptr_t>(mod));
    std::uint32_t* addrs = reinterpret_cast<std::uint32_t*>(export_section->AddressOfFunctions 
      + reinterpret_cast<std::uintptr_t>(mod));

    const auto module_base = reinterpret_cast<std::uintptr_t>(mod);

    for (std::uint32_t i = 0; i < export_section->NumberOfFunctions; i++) {
      char* function_name = i < export_section->NumberOfNames ?
        reinterpret_cast<char*>(names[i] + 
          reinterpret_cast<std::uintptr_t>(mod)) :
        nullptr;

      std::uint16_t ordinal = function_name ? ordinals[i] : i;
      auto addr = addrs[ordinal] + reinterpret_cast<std::uintptr_t>(mod);


      if (function_name)
        data["Exports"][function_name]
          = reinterpret_cast<std::uintptr_t>(GetProcAddress(mod, function_name));
      continue;
      if (addr >= module_base + export_data_dir && addr <= module_base + export_data_dir
        + export_data_size) {

        std::string forward = reinterpret_cast<const char*>(addr);

        forward = forward.c_str();
        auto dll_name = forward.substr(0, forward.find(".")) + (".dll");
        auto func_name = forward.substr( forward.find(".") + 1);

        if(!temp_module_data["modules"].count(dll_name))
          dump_module(LoadLibraryA(dll_name.c_str()), temp_module_data["modules"][dll_name]);

        data["Exports"][function_name] = temp_module_data["modules"][dll_name]["Exports"][func_name];
      }
    }
  }
}

int main(int argc, char** argv) {

  HANDLE proc_handle = OpenProcess(PROCESS_ALL_ACCESS,
    FALSE, GetCurrentProcessId());
  LPVOID base_addr = VirtualAllocEx(proc_handle, NULL, 0x3000000,
    MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
  
  module_data["allocated"] = reinterpret_cast<std::uintptr_t>(base_addr);
  dump_module(LoadLibraryA("USER32.dll"), module_data["modules"]["USER32.dll"]);
  dump_module(LoadLibraryA("KERNEL32.dll"), module_data["modules"]["KERNEL32.dll"]);

  {
    std::fstream file("exports.txt",
      std::ios::trunc | std::ios::binary | std::ios::out);

    auto json_str = module_data.dump();
    file.write(json_str.data(), json_str.size());
  }

  system("pause");

  std::fstream dll("hack_patched.dll",
    std::ios::in | std::ios::binary | std::ios::ate);
  auto size = dll.tellg();
  dll.seekg(0, std::ios::beg);
  
  dll.read(reinterpret_cast<char*>(base_addr), size);
  auto th = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)((std::uint8_t*)base_addr + 0xa00), 0, 0, 0);
  
  std::this_thread::sleep_for(std::chrono::seconds(2));
  WaitForSingleObject(th, INFINITE);
  MessageBox(0, 0, 0, 0);
  CloseHandle(th);
  CloseHandle(proc_handle);
  return 0;
}